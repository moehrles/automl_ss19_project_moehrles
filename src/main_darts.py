import os
import sys
import numpy as np
import time
import torch
import utils
import logging
import argparse
import datetime
import torch.nn as nn
import torch.nn.functional as F
import torchvision.transforms as transforms
from torch.utils.data import DataLoader

from datasets import K49, KMNIST
from torch.autograd import Variable
from darts import train_search, train
from darts.genotypes import Genotype


def main(args):
    logging.info("args = %s", args)

    # Device configuration
    if torch.cuda.is_available():
        device = torch.device('cuda:0')
        logging.info('Device: cuda')
    else:
        device = torch.device('cpu')
        logging.info('Device: cpu')

    # You can add any preprocessing/data augmentation you want here
    if args.genotype:
        genotype = eval(args.genotype)
        logging.info('Genotype loaded: %s', args.genotype)
    else:
        train_augmentations, test_augmentations = utils.data_augmentations(args)

        train_dataset = KMNIST(args.data, True, train_augmentations)
        test_dataset = KMNIST(args.data, False, test_augmentations)

        genotype, acc = train_search.main(args, device, train_dataset, test_dataset)
        logging.info('Best genotype (with validation accuracy: %f): %s', acc, genotype)

    if args.genotype or args.retrain:
        train_augmentations, test_augmentations = utils.data_augmentations(args)

        train_dataset = K49(args.data, True, train_augmentations)
        test_dataset = K49(args.data, False, test_augmentations)

        # If num_train_seeds is zero do a normal train run
        if args.num_train_seeds == 0:
            final_acc = train.main(genotype, args, device, train_dataset, test_dataset)
        else:
            seeds = np.random.choice(int(1e5), args.num_train_seeds, replace=False)
            total_epochs = args.epochs
            seed_search_epochs = 2
            args.epochs = seed_search_epochs

            best_acc = None
            best_seed = None
            for seed in seeds:
                args.seed = seed
                valid_acc = train.main(genotype, args, device, train_dataset, test_dataset)
                if best_acc is None or valid_acc > best_acc:
                    best_acc = valid_acc
                    best_seed = seed

            args.epochs = total_epochs - seed_search_epochs
            args.seed = best_seed
            final_acc = train.main(genotype, args, device, train_dataset, test_dataset, warm_start=True)

        logging.info('Final accuracy: %f', final_acc)


if __name__ == '__main__':
    parser = argparse.ArgumentParser("Darts")
    parser.add_argument('--data', type=str, default='../data', help='location of the data corpus')
    parser.add_argument('--batch_size', type=int, default=64, help='batch size')
    parser.add_argument('--learning_rate', type=float, default=0.025, help='init learning rate')
    parser.add_argument('--weight_decay', type=float, default=3e-4, help='weight decay')
    parser.add_argument('--momentum', type=float, default=0.9, help='momentum')
    parser.add_argument('--epochs', type=int, default=20, help='num of training epochs')
    parser.add_argument('--save', type=str, default='logs', help='path to logs')
    parser.add_argument('--seed', type=int, default=2, help='random seed')
    parser.add_argument('--layers_search', type=int, default=8, help='Number of Cells while searching for an architecture')
    parser.add_argument('--layers_train', type=int, default=20, help='Number of Cells while training the final architecture')
    parser.add_argument('--nodes', type=int, default=4, help='Number of nodes in a cell')
    parser.add_argument('--cutout', action='store_true', default=False, help='use cutout')
    parser.add_argument('--retrain', action='store_true', default=False, help='retrain found genotype')
    parser.add_argument('--cutout_length', type=int, default=16, help='cutout length')
    parser.add_argument('--train_portion', type=float, default=0.5, help='portion of training data')
    parser.add_argument('--grad_clip', type=float, default=5, help='gradient clipping')
    parser.add_argument('--drop_path_prob', type=float, default=0.2, help='drop path probability')
    parser.add_argument('--genotype', type=str, default='', help='Skip genotype search and directly train with this genotype')
    parser.add_argument('--init_channels_search', type=int, default=4, help='num of init channels while searching for an architecture')
    parser.add_argument('--init_channels_train', type=int, default=8, help='num of init channels while training the final architecture')
    parser.add_argument('--num_train_seeds', type=int, default=0, help='Number of seeds which will be evaluated for the train stage. If 0 then the given seed is used')

    args = parser.parse_args()

    # logging utilities
    os.makedirs(args.save, exist_ok=True)
    log_format = '%(asctime)s %(message)s'
    logging.basicConfig(stream=sys.stdout, level=logging.INFO,
                        format=log_format, datefmt='%m/%d %I:%M:%S %p')
    fh = logging.FileHandler(os.path.join(args.save, 'log.txt'))
    fh.setFormatter(logging.Formatter(log_format))
    logging.getLogger().addHandler(fh)

    main(args)
