import torch
import torch.nn as nn

class Architect(object):
    """
    Base class for the architect, which is just an optimizer (different from
    the one used to update the model parameters) for the architectural parameters.
    """
    def __init__(self, model):
        """
        :model: nn.Module; search model
        """
        self.model = model
        self.criterion = nn.CrossEntropyLoss()
        self.arch_optimizer = torch.optim.Adam(
                self.model.arch_parameters,
                lr=3e-4,
                betas=(0.5, 0.999),
                weight_decay=1e-3
        )

    def step(self, input_valid, target_valid):
        """
        This method computes a gradient step in the architecture space, i.e.
        updates the self.model.alphas_normal and self.model.alphas_reduce by
        the gradient of the validation loss with respect to these alpha
        parameters.
        :input_valid: torch.Tensor; validation mini-batch
        :target_valid: torch.Tensor: ground truth labels of this mini-batch
        """
        self.arch_optimizer.zero_grad()
        logits = self.model(input_valid)
        loss = self.criterion(logits, target_valid)
        loss.backward()
        self.arch_optimizer.step()
