from collections import namedtuple

from darts.operations import OPS

# this object will be needed to represent the discrete architecture extracted
# from the architectural parameters. See the method genotype() below.
Genotype = namedtuple('Genotype', 'normal normal_concat reduce reduce_concat')

PRIMITIVES = list(OPS.keys())  # operations set as list of strings
