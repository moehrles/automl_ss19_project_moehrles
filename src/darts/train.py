import os
import sys
import time
import glob
import numpy as np
import torch
import utils
import logging
import argparse
import torch.nn as nn
import torch.utils
import torchvision.datasets as dset
import torch.backends.cudnn as cudnn


from torchsummary import summary
from torch.autograd import Variable
from darts.model import  Network


def main(genotype, args, device, train_data, test_data, warm_start=False):
  """
  Main entrypoint for train. Retrains the found genotyp from scratch

  Args:
  - seed: Random seed
  - layers
  - learning_rate
  - momentum
  - weight_decay
  - batch_size
  - epochs
  - save
  - grad_clip
  - drop_path_prob
  """

  np.random.seed(args.seed)
  torch.manual_seed(args.seed)
  torch.cuda.manual_seed(args.seed)

  cudnn.benchmark = True
  cudnn.enabled = True

  criterion = nn.CrossEntropyLoss().to(device)
  model = Network(args.init_channels_train, train_data.n_classes, args.layers_train, genotype, stem_multiplier=train_data.channels).to(device)

  kwargs = {'num_workers': 2, 'pin_memory': True}
  train_queue = torch.utils.data.DataLoader(
      train_data, batch_size=args.batch_size, shuffle=True)

  test_queue = torch.utils.data.DataLoader(
      test_data, batch_size=args.batch_size, shuffle=False)

  if warm_start:
    logging.info("Weights loaded")
    utils.load(model, os.path.join(args.save, 'weights_{0}.pt'.format(args.seed)))
    model.drop_path_prob = args.drop_path_prob
    infer(test_queue, model, criterion, device)

  # model.drop_path_prob = 0
  # summary(
  #     model, (train_data.channels,
  #             train_data.img_rows,
  #             train_data.img_cols),
  #     device='cuda' if torch.cuda.is_available() else 'cpu')

  # exit(0)

  logging.info("param size = %fMB, seed = %d", utils.count_parameters_in_MB(model), args.seed)

  optimizer = torch.optim.SGD(
      model.parameters(),
      args.learning_rate,
      momentum=args.momentum,
      weight_decay=args.weight_decay
      )

  scheduler = torch.optim.lr_scheduler.CosineAnnealingLR(optimizer, float(args.epochs))

  best_acc = None
  for epoch in range(args.epochs):
    logging.info("Starting epoch %d/%d, scheduler: %e", epoch+1, args.epochs, scheduler.get_lr()[0])
    start_time = time.time()

    model.drop_path_prob = args.drop_path_prob * epoch / args.epochs

    train_acc, _ = train(train_queue, model, criterion, optimizer, device, args)
    # logging.info('train_acc %f', train_acc)

    test_acc, _ = infer(test_queue, model, criterion, device)
    # logging.info('test_acc %f', test_acc)

    if best_acc is None or test_acc > best_acc:
      best_acc = test_acc
      utils.save(model, os.path.join(args.save, 'weights_{0}.pt'.format(args.seed)))

    elapsed_time = time.time() - start_time
    time_str = time.strftime("%H:%M:%S", time.gmtime(elapsed_time))
    logging.info("Duration: %s", time_str)

    scheduler.step()

  return best_acc

def train(train_queue, model, criterion, optimizer, device, args):
  objs = utils.AvgrageMeter()
  top1 = utils.AvgrageMeter()
  top5 = utils.AvgrageMeter()
  model.train()

  for step, (input, target) in enumerate(train_queue):
    input, target = input.to(device), target.to(device, non_blocking=True)


    optimizer.zero_grad()
    logits = model(input)
    loss = criterion(logits, target)

    loss.backward()
    nn.utils.clip_grad_norm_(model.parameters(), args.grad_clip)
    optimizer.step()

    prec1, prec5 = utils.accuracy(logits, target, topk=(1, 5))
    n = input.size(0)
    objs.update(loss.item(), n)
    top1.update(prec1.item(), n)
    top5.update(prec5.item(), n)

    if False and step % 10 == 0:
      logging.info('train mini-batch %03d, loss=%e accuracy=%f top5=%f', step, objs.avg, top1.avg, top5.avg)

  logging.info('train mini-batch %03d, loss=%e accuracy=%f top5=%f', step, objs.avg, top1.avg, top5.avg)
  return top1.avg, objs.avg


def infer(valid_queue, model, criterion, device):
  objs = utils.AvgrageMeter()
  top1 = utils.AvgrageMeter()
  top5 = utils.AvgrageMeter()
  model.eval()

  with torch.no_grad():
    for step, (input, target) in enumerate(valid_queue):
      input, target = input.to(device), target.to(device, non_blocking=True)

      logits = model(input)
      loss = criterion(logits, target)

      prec1, prec5 = utils.accuracy(logits, target, topk=(1, 5))
      n = input.size(0)
      objs.update(loss.item(), n)
      top1.update(prec1.item(), n)
      top5.update(prec5.item(), n)

      if False and step % 10 == 0:
        logging.info('valid mini-batch %03d, loss=%e accuracy=%f top5=%f', step, objs.avg, top1.avg, top5.avg)

  logging.info('valid mini-batch %03d, loss=%e accuracy=%f top5=%f', step, objs.avg, top1.avg, top5.avg)

  return top1.avg, objs.avg
