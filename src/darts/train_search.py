import numpy as np
import torch
import time
import utils
import logging
import torch.nn as nn
import torch.utils
import torch.nn.functional as F
import torch.backends.cudnn as cudnn

from tqdm import tqdm
from torch.autograd import Variable

from darts.model_search import Network
from darts.architect import Architect


def main(args, device, train_data, test_data):
  """
  Main entrypoint for train_search. Returns the best found genotype

  Args:
  - seed: Random seed
  - layers
  - nodes: Number of nodes in a cell
  - learning_rate
  - momentum
  - weight_decay
  - batch_size
  - epochs
  - save
  - grad_clip
  """
  np.random.seed(args.seed)
  torch.manual_seed(args.seed)
  torch.cuda.manual_seed(args.seed)

  cudnn.benchmark = True
  cudnn.enabled = True

  criterion = nn.CrossEntropyLoss().to(device)
  model = Network(args.init_channels_search, train_data.n_classes, args.layers_search, args.nodes, device, stem_multiplier=train_data.channels).to(device)

  logging.info("param size = %fMB", utils.count_parameters_in_MB(model))

  optimizer = torch.optim.SGD(
      model.parameters(),
      args.learning_rate,
      momentum=args.momentum,
      weight_decay=args.weight_decay)


  num_train = len(train_data)
  indices = list(range(num_train))
  split = int(np.floor(args.train_portion * num_train))

  kwargs = {'num_workers': 0, 'pin_memory': True}
  train_loader = torch.utils.data.DataLoader(
            train_data, batch_size=args.batch_size,
            sampler=torch.utils.data.sampler.SubsetRandomSampler(indices[:split]),
            **kwargs)
  arch_loader = torch.utils.data.DataLoader(
            train_data, batch_size=args.batch_size,
            sampler=torch.utils.data.sampler.SubsetRandomSampler(indices[split:]),
            **kwargs)
  valid_loader = torch.utils.data.DataLoader(
            test_data, batch_size=args.batch_size,
            sampler=torch.utils.data.sampler.SequentialSampler(test_data),
            **kwargs)

  architect = Architect(model)

  best_acc = None
  best_geno = None
  for epoch in range(args.epochs):
    logging.info("Starting epoch %d/%d", epoch+1, args.epochs)
    start_time = time.time()

    # training
    train_acc, _ = train(train_loader, arch_loader, model,
            architect, criterion, optimizer, device, args, train_arch=epoch>4)
    # logging.info('train_acc %f', train_acc)

    # validation
    valid_acc, _ = infer(valid_loader, model, criterion, device)
    # logging.info('valid_acc %f', valid_acc)

    # compute the discrete architecture from the current alphas
    genotype = model.genotype()
    # logging.info('genotype = %s', genotype)

    print(F.softmax(model.alphas_normal, dim=-1))
    print(F.softmax(model.alphas_reduce, dim=-1))

    if best_acc is None or valid_acc > best_acc:
      best_acc = valid_acc
      best_geno = genotype

    elapsed_time = time.time() - start_time
    time_str = time.strftime("%H:%M:%S", time.gmtime(elapsed_time))
    logging.info("Duration: %s", time_str)
    with open(args.save + '/architecture', 'a+') as f:
        f.write(str(genotype) + '\n')

  return best_geno, best_acc

def train(train_queue, valid_queue, model, architect, criterion, optimizer, device, args, train_arch=True):
  objs = utils.AvgrageMeter()
  top1 = utils.AvgrageMeter()
  top5 = utils.AvgrageMeter()

  model.train()
  # t = tqdm(train_queue)
  for step, (input, target) in enumerate(train_queue):
    n = input.size(0)

    input, target = input.to(device), target.to(device)

    # get a random minibatch from the search queue with replacement
    if train_arch:
      input_search, target_search = next(iter(valid_queue))
      architect.step(input_search.to(device), target_search.to(device))

    optimizer.zero_grad()
    logits = model(input)
    loss = criterion(logits, target)

    loss.backward()
    nn.utils.clip_grad_norm_(model.parameters(), args.grad_clip)
    optimizer.step()

    prec1, prec5 = utils.accuracy(logits, target, topk=(1, 5))
    objs.update(loss.item(), n)
    top1.update(prec1.item(), n)
    top5.update(prec5.item(), n)

    if False and step % 1000 == 0:
      logging.info('train mini-batch %03d, loss=%e accuracy=%f top5=%f', step, objs.avg, top1.avg, top5.avg)

  logging.info('train mini-batch %03d, loss=%e accuracy=%f top5=%f, train_arch=%s', step, objs.avg, top1.avg, top5.avg, str(train_arch))
  return top1.avg, objs.avg


def infer(valid_queue, model, criterion, device):
  objs = utils.AvgrageMeter()
  top1 = utils.AvgrageMeter()
  top5 = utils.AvgrageMeter()
  model.eval()

  with torch.no_grad():
    for step, (input, target) in enumerate(valid_queue):
      input, target = input.to(device), target.to(device)

      logits = model(input)
      loss = criterion(logits, target)

      prec1, prec5 = utils.accuracy(logits, target, topk=(1, 5))
      n = input.size(0)
      objs.update(loss.item(), n)
      top1.update(prec1.item(), n)
      top5.update(prec5.item(), n)

      if False and step % 1000 == 0:
        logging.info('valid mini-batch %03d, loss=%e accuracy=%f top5=%f', step, objs.avg, top1.avg, top5.avg)

  logging.info('valid mini-batch %03d, loss=%e accuracy=%f top5=%f', step, objs.avg, top1.avg, top5.avg)
  return top1.avg, objs.avg
