#!/bin/bash
#SBATCH -p meta_gpu-black # partition (queue)
#SBATCH --mem 4000 # memory pool for all cores (4GB)
#SBATCH -t 1-01:00 # time (D-HH:MM)
#SBATCH -c 1 # number of cores
#SBATCH --gres=gpu:1
#SBATCH -o ../log/%x.%j.out # STDOUT  (the folder log has to be created prior to running or this won't work)
#SBATCH -e ../log/%x.%j.err # STDERR  (the folder log has to be created prior to running or this won't work)
#SBATCH --mail-type=END,FAIL # (recive mails about end and timeouts/crashes of your job)
# Print some information about the job to STDOUT

PRJ="$HOME/automl_ss19_project_moehrles"
source "$PRJ/venv/bin/activate"

echo "Workingdir: $PWD";
echo "Started at $(date)";
echo "Running job $SLURM_JOB_NAME using $SLURM_JOB_CPUS_PER_NODE cpus per node with given JID $SLURM_JOB_ID on queue $SLURM_JOB_PARTITION"; 

# Job to perform
python "$PRJ/src/main_darts.py" --save="logs_$SLURM_JOB_ID"


# Print some Information about the end-time to STDOUT
echo "DONE";
echo "Finished at $(date)";
